package com.example.admin.picture;

import com.example.admin.AdminApplication;
import com.example.admin.picture.root.PictureRoot;
import com.example.core.domain.enititys.user.service.UserService;
import org.apache.commons.compress.utils.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.io.FileInputStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = AdminApplication.class)
@AutoConfigureMockMvc
public class PictureTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithUserDetails("Admin")
    public void addTest() throws Exception {
        File file = new File("src/test/resources/static/picture/Screenshot_79.png");
        FileInputStream inputStream = new FileInputStream(file);

        mockMvc.perform(multipart("/" + PictureRoot.ADD_PICTURE)
                .file(new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(inputStream)))
                .param("title","test_file"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }
}
