package com.example.admin.auth.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthTestModel {
    private String username;

    private String password;
}
