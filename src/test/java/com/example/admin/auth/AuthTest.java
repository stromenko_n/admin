package com.example.admin.auth;

import com.example.admin.AdminApplication;
import com.example.admin.auth.api.request.RegistrationRequest;
import com.example.admin.auth.root.RegistrationRoot;
import com.example.core.base.root.AdminRoot;
import com.example.core.data.user.entity.UserDoc;
import com.example.core.data.user.model.Role;
import com.example.core.data.user.model.list.Roles;
import com.example.core.domain.enititys.user.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = AdminApplication.class)
@AutoConfigureMockMvc
public class AuthTest {
    @Autowired
    private UserService userService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void registrationTest() throws Exception {
        RegistrationRequest request = new RegistrationRequest("Admin", "Admin", "Admin");

        mockMvc.perform(post("/" + RegistrationRoot.REGISTRATION)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("OK!")));
    }

    @Test
    public void authTest() throws Exception {
        createUser();
        mockMvc.perform(post("/" + AdminRoot.ADMIN + "/login")
                .param("username", "Admin")
                .param("password", "Admin"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/" + AdminRoot.ADMIN));
    }

    private void createUser() {
        UserDoc userDoc = new UserDoc();
        userDoc.setUsername("Admin");
        userDoc.setPassword("Admin");
        userDoc.setRoles(Collections.singleton(new Role(Roles.ADMIN.toString())));

        userService.saveUser(userDoc);
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
