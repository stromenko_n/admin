package com.example.admin.product.factory;

import com.example.admin.product.api.request.ProductAdminRequest;
import com.example.core.data.product.model.ProductDTO;
import com.example.core.domain.enititys.product.service.ProductService;
import org.bson.types.ObjectId;

import java.util.Collections;

public class ProductTestFactory {

    public static void createFiveProducts(ProductService service) {
        service.dropCollection();
        service.create(new ProductDTO(null, "name1", "desc1", null, Collections.emptyList(), null, null));
        service.create(new ProductDTO(null, "name2", "desc2", null, Collections.emptyList(), null, null));
        service.create(new ProductDTO(null, "name3", "desc3", null, Collections.emptyList(), null, null));
        service.create(new ProductDTO(null, "name4", "desc4", null, Collections.emptyList(), null, null));
        service.create(new ProductDTO(null, "name5", "desc5", null, Collections.emptyList(), null, null));
    }

    public static String createFiveProductsForFindOne(ProductService service) {
        service.dropCollection();
        String id = ObjectId.get().toString();

        service.create(new ProductDTO(id, "name1", "desc1", null, Collections.emptyList(), null, null));
        service.create(new ProductDTO(ObjectId.get().toString(), "name2", "desc2", null, Collections.emptyList(), null, null));
        service.create(new ProductDTO(ObjectId.get().toString(), "name3", "desc3", null, Collections.emptyList(), null, null));
        service.create(new ProductDTO(ObjectId.get().toString(), "name4", "desc4", null, Collections.emptyList(), null, null));
        service.create(new ProductDTO(ObjectId.get().toString(), "name5", "desc5", null, Collections.emptyList(), null, null));

        return id;
    }

    public static ProductAdminRequest createRequest() {
        ProductAdminRequest request = new ProductAdminRequest();

        request.setId(null);
        request.setName("test");
        request.setDescription("test1");
        request.setMainPicId(null);
        request.setPics(Collections.emptyList());
        request.setPrice(null);

        return request;
    }

    public static ProductAdminRequest createRequest(String id) {
        ProductAdminRequest request = new ProductAdminRequest();

        request.setId(id);
        request.setName("test");
        request.setDescription("test1");
        request.setMainPicId(null);
        request.setPics(Collections.emptyList());
        request.setPrice(null);

        return request;
    }
}
