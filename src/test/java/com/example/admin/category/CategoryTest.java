package com.example.admin.category;

import com.example.admin.AdminApplication;
import com.example.admin.category.factory.CategoryTestFactory;
import com.example.admin.category.root.CategoryAdminRoot;
import com.example.core.data.category.model.CategoryDTO;
import com.example.core.domain.enititys.category.service.CategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = AdminApplication.class)
@AutoConfigureMockMvc
public class CategoryTest {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithUserDetails("Admin")
    public void findAll() throws Exception {
        CategoryTestFactory.createFiveCategories(categoryService);

        mockMvc.perform(get("/" + CategoryAdminRoot.FIND_ALL))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$.[*].name", contains("name1", "name2", "name3", "name4", "name5")));
    }

    @Test
    @WithUserDetails("Admin")
    public void findOne() throws Exception {
        String id = CategoryTestFactory.createFiveCategoriesForFindOne(categoryService);

        mockMvc.perform(get("/" + CategoryAdminRoot.FIND_ONE.replace("{id}", id)))
                .andExpect(jsonPath("$.name", is("name1")));
    }

    @Test
    @WithUserDetails("Admin")
    public void createTest() throws Exception {
        mockMvc.perform(post("/" + CategoryAdminRoot.CREATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(CategoryTestFactory.createRequest())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("OK!")));
    }

    @Test
    @WithUserDetails("Admin")
    public void updateTest() throws Exception {
        String id = CategoryTestFactory.createFiveCategoriesForFindOne(categoryService);

        mockMvc.perform(put("/" + CategoryAdminRoot.UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(CategoryTestFactory.createRequest(id))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("OK!")));

        List<CategoryDTO> all = categoryService.findAll();
        Optional<CategoryDTO> first = all.stream()
                .filter(c -> c.getName().equals("test"))
                .findFirst();

        Assertions.assertEquals(5, all.size());
        Assertions.assertTrue(first.isPresent());
    }

    @Test
    @WithUserDetails("Admin")
    public void deleteTest() throws Exception {
        String id = CategoryTestFactory.createFiveCategoriesForFindOne(categoryService);

        mockMvc.perform(delete("/" + CategoryAdminRoot.DELETE.replace("{id}", id)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("OK!")));

        List<CategoryDTO> all = categoryService.findAll();
        Optional<CategoryDTO> first = all.stream()
                .filter(c -> c.getName().equals("name1"))
                .findFirst();

        Assertions.assertEquals(4, all.size());
        Assertions.assertFalse(first.isPresent());
    }


    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
