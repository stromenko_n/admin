package com.example.admin.category.factory;

import com.example.admin.category.api.request.CategoryAdminRequest;
import com.example.core.data.category.model.CategoryDTO;
import com.example.core.domain.enititys.category.service.CategoryService;
import org.bson.types.ObjectId;

public class CategoryTestFactory {

    public static void createFiveCategories(CategoryService service) {
        service.dropCollection();
        service.create(new CategoryDTO(null, "name1", "desc1", null, null));
        service.create(new CategoryDTO(null, "name2", "desc2", null, null));
        service.create(new CategoryDTO(null, "name3", "desc3", null, null));
        service.create(new CategoryDTO(null, "name4", "desc4", null, null));
        service.create(new CategoryDTO(null, "name5", "desc5", null, null));
    }

    public static String createFiveCategoriesForFindOne(CategoryService service) {
        service.dropCollection();
        String id = ObjectId.get().toString();

        service.create(new CategoryDTO(id, "name1", "desc1", null, null));
        service.create(new CategoryDTO(ObjectId.get().toString(), "name2", "desc2", null, null));
        service.create(new CategoryDTO(ObjectId.get().toString(), "name3", "desc3", null, null));
        service.create(new CategoryDTO(ObjectId.get().toString(), "name4", "desc4", null, null));
        service.create(new CategoryDTO(ObjectId.get().toString(), "name5", "desc5", null, null));

        return id;
    }

    public static CategoryAdminRequest createRequest() {
        CategoryAdminRequest request = new CategoryAdminRequest();
        request.setId(null);
        request.setName("test");
        request.setDescription("desc");
        request.setParentId(null);
        request.setPicId(null);

        return request;
    }

    public static CategoryAdminRequest createRequest(String id) {
        CategoryAdminRequest request = new CategoryAdminRequest();
        request.setId(id);
        request.setName("test");
        request.setDescription("desc");
        request.setParentId(null);
        request.setPicId(null);

        return request;
    }
}
