package com.example.admin.auth.controller;

import com.example.admin.auth.api.exceptions.RegistrationExceptions;
import com.example.admin.auth.api.request.RegistrationRequest;
import com.example.admin.auth.facade.RegistrationFacade;
import com.example.admin.auth.root.RegistrationRoot;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RegistrationController {
    private final RegistrationFacade facade;

    @PostMapping(RegistrationRoot.REGISTRATION)
    public String addUser(@RequestBody RegistrationRequest registrationRequest) throws RegistrationExceptions {
        return facade.addUser(registrationRequest);
    }
}
