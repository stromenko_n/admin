package com.example.admin.auth.root;

import com.example.core.base.root.AdminRoot;

public class RegistrationRoot {

    public static final String REGISTRATION = AdminRoot.ADMIN + "/registration";

}
