package com.example.admin.auth.facade.impl;

import com.example.admin.auth.api.exceptions.RegistrationExceptions;
import com.example.admin.auth.api.request.RegistrationRequest;
import com.example.admin.auth.facade.RegistrationFacade;
import com.example.core.data.user.entity.UserDoc;
import com.example.core.data.user.model.Role;
import com.example.core.data.user.model.list.Roles;
import com.example.core.domain.enititys.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class RegistrationFacadeImpl implements RegistrationFacade {
    private final UserService service;

    @Override
    public String addUser(RegistrationRequest request) throws RegistrationExceptions {
        if (!request.checkTheCorrect()){
            throw new RegistrationExceptions("Passwords don't match");
        } else if (service.saveUser(userFactory(request))) {
            return "OK!";
        }
        throw new RegistrationExceptions("This username exists");
    }

    private UserDoc userFactory(RegistrationRequest request) {
        UserDoc userDoc = new UserDoc();
        userDoc.setUsername(request.getUsername());
        userDoc.setPassword(request.getPassword());
        userDoc.setRoles(Collections.singleton(new Role(Roles.USER.toString())));

        return userDoc;
    }
}
