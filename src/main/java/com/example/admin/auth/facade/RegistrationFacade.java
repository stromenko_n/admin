package com.example.admin.auth.facade;

import com.example.admin.auth.api.exceptions.RegistrationExceptions;
import com.example.admin.auth.api.request.RegistrationRequest;

public interface RegistrationFacade {

    String addUser(RegistrationRequest request) throws RegistrationExceptions;
}
