package com.example.admin.auth.api.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationRequest {
    private String username;

    private String password;

    private String confirmPassword;

    public boolean checkTheCorrect() {
        return password.equals(confirmPassword);
    }
}
