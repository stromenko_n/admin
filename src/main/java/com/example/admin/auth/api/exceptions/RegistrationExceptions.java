package com.example.admin.auth.api.exceptions;

import com.example.admin.auth.api.request.RegistrationRequest;

public class RegistrationExceptions extends Exception {

    public RegistrationExceptions(String message) {
        super(message);
    }
}
