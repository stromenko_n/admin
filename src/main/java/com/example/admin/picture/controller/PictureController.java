package com.example.admin.picture.controller;

import com.example.admin.picture.facade.PictureFacade;
import com.example.admin.picture.root.PictureRoot;
import com.example.core.data.picture.exception.PictureException;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequiredArgsConstructor
public class PictureController {
    private final PictureFacade facade;

    @ResponseBody
    @PostMapping(value = PictureRoot.ADD_PICTURE)
    public String addPicture(@RequestParam String title, @RequestParam MultipartFile file) throws IOException {
        return facade.addPicture(title, file);
    }

    @GetMapping(value = PictureRoot.GET_PICTURE)
    public void getPicture(@PathVariable ObjectId id,
                           HttpServletResponse response) throws IOException, PictureException {
        facade.getPicture(id, response);
    }
}
