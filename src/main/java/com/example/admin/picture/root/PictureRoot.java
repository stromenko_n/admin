package com.example.admin.picture.root;

import com.example.core.base.root.AdminRoot;

public class PictureRoot {

    public static final String BASE = AdminRoot.ADMIN + "/picture";

    public static final String ADD_PICTURE = BASE + "/add";

    public static final String GET_PICTURE = BASE + "/get_picture/{id}";
}
