package com.example.admin.picture.facade;

import com.example.core.data.picture.exception.PictureException;
import org.bson.types.ObjectId;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface PictureFacade {

    String addPicture(String title, MultipartFile file) throws IOException;

    void getPicture(ObjectId id, HttpServletResponse response) throws PictureException, IOException;
}
