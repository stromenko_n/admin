package com.example.admin.picture.facade.impl;

import com.example.admin.picture.facade.PictureFacade;
import com.example.core.data.picture.entity.Picture;
import com.example.core.data.picture.exception.PictureException;
import com.example.core.domain.enititys.picture.service.PictureService;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
@RequiredArgsConstructor
public class PictureFacadeImpl implements PictureFacade {
    private final PictureService service;

    @Override
    public String addPicture(String title, MultipartFile file) throws IOException {
        return service.addPicture(title, file);
    }

    @Override
    public void getPicture(ObjectId id, HttpServletResponse response) throws PictureException, IOException {
        Picture picture = service.getPicture(id);

        InputStream in = new ByteArrayInputStream(picture.getImage().getData());
        IOUtils.copy(in, response.getOutputStream());
        response.flushBuffer();
    }
}
