package com.example.admin.product.api.request;

import com.example.core.data.product.model.ProductDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductAdminRequest {
    private String id;

    private String name;
    private String description;

    private String mainPicId;
    private List<String> pics;
    private String categoryId;

    private BigDecimal price;

    public ProductDTO toDto() {
        return new ProductDTO(
                id,
                name,
                description,
                mainPicId,
                pics,
                categoryId,
                price);
    }
}
