package com.example.admin.product.root;

import com.example.core.base.root.AdminRoot;

public class ProductAdminRoot {
    public static final String BASE = AdminRoot.ADMIN + "/product";

    public static final String FIND_ALL = BASE + "/find_all";
    public static final String FIND_ONE = BASE + "/find/{id}";
    public static final String CREATE = BASE + "/create";
    public static final String UPDATE = BASE + "/update";
    public static final String DELETE = BASE + "/delete/{id}";
}
