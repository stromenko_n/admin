package com.example.admin.product.controller;

import com.example.admin.product.api.request.ProductAdminRequest;
import com.example.admin.product.facade.ProductAdminFacade;
import com.example.admin.product.root.ProductAdminRoot;
import com.example.core.data.product.exception.ProductException;
import com.example.core.data.product.model.ProductDTO;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ProductAdminController {
    private final ProductAdminFacade facade;

    @GetMapping(ProductAdminRoot.FIND_ALL)
    public List<ProductDTO> findAll() {
        return facade.findAll();
    }

    @GetMapping(ProductAdminRoot.FIND_ONE)
    public ProductDTO findOne(@PathVariable ObjectId id) throws ProductException {
        return facade.findOne(id);
    }

    @PostMapping(ProductAdminRoot.CREATE)
    public String create(@RequestBody ProductAdminRequest request) {
        return facade.create(request);
    }

    @PutMapping(ProductAdminRoot.UPDATE)
    public String update(@RequestBody ProductAdminRequest request) throws ProductException {
        return facade.update(request);
    }

    @DeleteMapping(ProductAdminRoot.DELETE)
    public String delete(@PathVariable ObjectId id) throws ProductException {
        return facade.delete(id);
    }
}
