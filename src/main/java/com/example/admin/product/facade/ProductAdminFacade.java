package com.example.admin.product.facade;

import com.example.admin.product.api.request.ProductAdminRequest;
import com.example.core.data.product.exception.ProductException;
import com.example.core.data.product.model.ProductDTO;
import org.bson.types.ObjectId;

import java.util.List;

public interface ProductAdminFacade {

    List<ProductDTO> findAll();
    ProductDTO findOne(ObjectId id) throws ProductException;

    String create(ProductAdminRequest request);
    String update(ProductAdminRequest request) throws ProductException;
    String delete(ObjectId id) throws ProductException;
}
