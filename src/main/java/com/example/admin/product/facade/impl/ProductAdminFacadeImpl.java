package com.example.admin.product.facade.impl;

import com.example.admin.product.api.request.ProductAdminRequest;
import com.example.admin.product.facade.ProductAdminFacade;
import com.example.core.data.product.exception.ProductException;
import com.example.core.data.product.model.ProductDTO;
import com.example.core.domain.enititys.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductAdminFacadeImpl implements ProductAdminFacade {
    private final ProductService service;

    @Override
    public List<ProductDTO> findAll() {
        return service.findAllProducts();
    }

    @Override
    public ProductDTO findOne(ObjectId id) throws ProductException {
        return service.findById(id);
    }

    @Override
    public String create(ProductAdminRequest request) {
        return service.create(request.toDto());
    }

    @Override
    public String update(ProductAdminRequest request) throws ProductException {
        return service.update(request.toDto());
    }

    @Override
    public String delete(ObjectId id) throws ProductException {
        return service.delete(id);
    }
}
