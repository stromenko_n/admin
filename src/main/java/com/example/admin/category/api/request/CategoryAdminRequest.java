package com.example.admin.category.api.request;

import com.example.core.data.category.model.CategoryDTO;
import lombok.Data;

@Data
public class CategoryAdminRequest {
    private String id;

    private String name;
    private String description;

    private String picId;
    private String parentId;

    public CategoryDTO toDto() {
        return new CategoryDTO(
                id,
                name,
                description,
                picId,
                parentId);
    }
}
