package com.example.admin.category.controller;

import com.example.admin.category.api.request.CategoryAdminRequest;
import com.example.admin.category.facade.CategoryAdminFacade;
import com.example.admin.category.root.CategoryAdminRoot;
import com.example.core.data.category.exception.CategoryException;
import com.example.core.data.category.model.CategoryDTO;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CategoryAdminController {
    private final CategoryAdminFacade facade;

    @GetMapping(CategoryAdminRoot.FIND_ALL)
    public List<CategoryDTO> findAll() {
        return facade.findAll();
    }

    @GetMapping(CategoryAdminRoot.FIND_ONE)
    public CategoryDTO findOne(@PathVariable ObjectId id) throws CategoryException {
        return facade.findOne(id);
    }

    @PostMapping(CategoryAdminRoot.CREATE)
    public String create(@RequestBody CategoryAdminRequest request) {
        return facade.create(request);
    }

    @PutMapping(CategoryAdminRoot.UPDATE)
    public String update(@RequestBody CategoryAdminRequest request) throws CategoryException {
        return facade.update(request);
    }

    @DeleteMapping(CategoryAdminRoot.DELETE)
    public String delete(@PathVariable ObjectId id) throws CategoryException {
        return facade.delete(id);
    }
}
