package com.example.admin.category.facade;

import com.example.admin.category.api.request.CategoryAdminRequest;
import com.example.core.data.category.exception.CategoryException;
import com.example.core.data.category.model.CategoryDTO;
import org.bson.types.ObjectId;

import java.util.List;

public interface CategoryAdminFacade {

    List<CategoryDTO> findAll();
    CategoryDTO findOne(ObjectId id) throws CategoryException;

    String create(CategoryAdminRequest request);
    String update(CategoryAdminRequest request) throws CategoryException;
    String delete(ObjectId id) throws CategoryException;
}
