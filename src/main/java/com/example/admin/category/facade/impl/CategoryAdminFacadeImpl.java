package com.example.admin.category.facade.impl;

import com.example.admin.category.api.request.CategoryAdminRequest;
import com.example.admin.category.facade.CategoryAdminFacade;
import com.example.core.data.category.exception.CategoryException;
import com.example.core.data.category.model.CategoryDTO;
import com.example.core.domain.enititys.category.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryAdminFacadeImpl implements CategoryAdminFacade {
    private final CategoryService service;

    @Override
    public List<CategoryDTO> findAll() {
        return service.findAll();
    }

    @Override
    public CategoryDTO findOne(ObjectId id) throws CategoryException {
        return service.findById(id);
    }

    @Override
    public String create(CategoryAdminRequest request) {
        return service.create(request.toDto());
    }

    @Override
    public String update(CategoryAdminRequest request) throws CategoryException {
        return service.update(request.toDto());
    }

    @Override
    public String delete(ObjectId id) throws CategoryException {
        return service.delete(id);
    }
}
